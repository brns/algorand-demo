================================
Algorand 2.0 Python Demo
================================

Project Idea
---------------

Currency Exchange
^^^^^^^^^^^^^^^^^^

Two custom currencies/coins that can be
exchanged with each other by a fixed (integer) rate, without
further involvement of the currency owner.

Components:
""""""""""""""""""

- 2 standard assets as coins (`c1`, `c2`)
- Smart contract signed by the currency owner

Contract:
""""""""""""""

The signed smart contract allows any account, that has opted-in on the assets,
to transfer assets from the exchange account
as long as the transfer is done in an *atomic transfer*
that consists of two transaction; such that either

- the first transaction transfers `x` of `c1` to the exchange account
- and the second transfers `x*rate` of `c2` from the exchange account
  
or

- the first transaction transfers `x*rate` of `c2` to the exchange account
- and the second transfers `x` of `c1` from the exchange account

In the atomic transaction either both or no transaction succeeds.

*Extension: Allow buying from both assets for a certain amount of algos.*

Files
^^^^^^^^
- `helpers.py` Helper functions for algorand
- `asset_exchange.py` The PyTeal source of the contract
- `asset_exchange_deploy.py` Demo: Creates the assets and smart contract
- `asset_transfer.py` Helper functions to create assets

Setup
----------------


To compile the smart contract you need
to `install a node <https://developer.algorand.org/docs/run-a-node/setup/install/>`_
in order to use the `goal` cli to compile the contracts.

Follow the instructions to install a node. Then connect and
sync with a network (takes some hours for the testnet).

Dependent on the system and setup you can use the following commands.

.. code:: bash
   ./goal node start -d ~/node/testnetdata

   ./goal node start -d ~/node/testnetdata

   ./goal node stop -d ~/node/testnetdata


Retrieve and set the API token
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to set the token and address of the node in order
to run the program. 

They values are located in the files `algod.token`
and `algod.net` in the data folder of the node.
Set them in a `.env` file in the projects root directory,
together with the absolute path to the `goal` executable.

.. code:: bash
   :name: .env
      
   ALGOD_TOKEN=<your algod token>
   ALGOD_ADDRESS=http://127.0.0.1:8080
   GOAL_PATH=/home/brns/node/goal


Install the dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^

`Poetry`_ can be used to install and build the project.

.. code:: bash

   poetry install 
   
Alternative the requirements are listed as well in `requirements.txt` 
for common `pip` installation.

.. code:: bash

   pip install -r requirements.txt

NB: Only tested with ``Python 3.8``.

.. _`Poetry`: https://python-poetry.org/docs/


Test the example
^^^^^^^^^^^^^^^^^^^

To deploy the contract and run the example use

.. code:: bash
   

   python algorand_demo/asset_exchange_deploy.py



Resources:
------------------------

- `Algorand Developer Docs`_
- `py-algorand-sdk`_
- `PureStake Examples`_

.. _`py-algorand-sdk`: https://github.com/algorand/py-algorand-sdk

.. _`Algorand Developer Docs`: https://developer.algorand.org/docs/

.. _`PureStake Examples`: https://github.com/PureStake/api-examples/tree/master/python-examples
