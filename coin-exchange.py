from algosdk import algod, transaction, account, mnemonic
from decouple import config
from pprint import pprint

# connect to the node
algod_client = algod.AlgodClient(config("ALGOD_TOKEN"), config("ALGOD_ADDRESS"))
params = algod_client.suggested_params()

# get suggested values for transactions
network_params = {
   "gh": params["genesishashb64"],
   "first": params["lastRound"],
   "last": params["lastRound"] + 1000, # > 1h
   "fee": params.get("minFee", 1000),
}

pprint(network_params)

def wait_for_confirmation(algod_client, txid):
    """Utility for waiting on a transaction confirmation."""
    while True:
        txinfo = algod_client.pending_transaction_info(txid)
        if txinfo.get("round") and txinfo.get("round") > 0:
            print(
                "Transaction {} confirmed in round {}.".format(
                    txid, txinfo.get("round")
                )
            )
            return txinfo
        else:
            print("Waiting for confirmation...")
            algod_client.status_after_block(algod_client.status().get("lastRound") + 1)

from algosdk import account, mnemonic
from pprint import pprint


def generate_algorand_keypair():
    private_key, address = account.generate_account()
    mnemonic_ = mnemonic.from_private_key(private_key)
    return {"pk": address, "sk": private_key, "mnemonic": mnemonic_}


alice = {
    "mnemonic": "approve purchase give hundred knee verify joy come biology "
    "office width point side visual brief gaze couple knife work "
    "paper proof blood today abandon acid",
    "pk": "SNWNEH25CIO3GDNMHUTPQK7SDRFER6QVGE6VPIYOE5EHRUMSRR6UI3UBWY",
    "sk": "VojrxPLGvcoTDy6zYKb1dfrj03+jYInpHvv/KdZfaByTbNIfXRIdsw2sPSb4K/IcSkj6FTE9V6MOJ0h40ZKMfQ==",
}

bob = {
    "mnemonic": "diesel piano glass hundred seven type toy auction course local "
    "route pass duck receive employ add keep average wet confirm hair "
    "round life absent south",
    "pk": "UZFECPY3NNSLVAWXVUVG7B4KOJNM7R7ZK4NZ7MLRS6JMU2IAYJ3AZS2RXM",
    "sk": "7AHpxfJWYq/P/A6K0SB5C+ohzh4pA87zA/PvIrTxKrCmSkE/G2tkuoLXrSpvh4pyWs/H+Vcbn7Fxl5LKaQDCdg==",
}

alice_ = algod_client.account_info(alice["pk"])
bob_ = algod_client.account_info(bob["pk"])

print("Alice: {}".format(alice["pk"]))
print("Bob: {}".format(bob["pk"]))

import ipdb; ipdb.set_trace()

def create_asset(asset_name, unit_name, account):
    data = {
        "sender": account["pk"],
        "total": 1000,
        "decimals": 0,
        "unit_name": unit_name,
        "asset_name": asset_name,
        "manager": account["pk"],  # re-configure, delete
        "reserve": account["pk"],  # receives the coins
        "freeze": "",              # disabled
        "clawback": "",            # disabled
        "url": "http://tiny.cc/lxd9oz",
        "flat_fee": True,
        "default_frozen": False,
        "strict_empty_address_check": False,
        **network_params,
    }

    print("Create {}".format(asset_name))
    txn = transaction.AssetConfigTxn(**data)
    stxn = txn.sign(account["sk"])

    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )

    txinfo = wait_for_confirmation(algod_client, txid)
    asset_id = txinfo["txresults"]["createdasset"]

    print("Created Asset: {} {}\n".format(asset_name, asset_id))

    return asset_id

def opt_in_asset(asset_id, account):
    """Allow this account to receive this asset.

    This is done by sending an `AssetTransferTransaction` 
    with amount 0 from and to the same account."""

    data = {
        "sender": account["pk"],
        "receiver": account["pk"],
        "amt": 0,
        "index": asset_id,
        "flat_fee": True,
        **network_params
    }

    print("Opt-in: {}".format(asset_id))

    txn = transaction.AssetTransferTxn(**data)
    stxn = txn.sign(account["sk"])
    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )

    wait_for_confirmation(algod_client, txid)
    print("Done\n")

def transfer_asset(asset_id, amount, sender, receiver):
    """Send the assets to the receiver."""

    data = {
        "sender": sender["pk"],
        "receiver": receiver["pk"],
        "amt": amount,
        "index": asset_id,
        "flat_fee": True,
        **network_params
    }

    print("Transfer: {} {}".format(amount, asset_id))

    txn = transaction.AssetTransferTxn(**data)
    stxn = txn.sign(sender["sk"])
    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )

    wait_for_confirmation(algod_client, txid)
    print("Done\n")

# Demo: Setup the coins 

# Create coins
pounds_id = create_asset("Pounds", "p", alice)
shillings_id = create_asset("Shillings", "s", alice)

# Allow bob to receive the assets
opt_in_asset(pounds_id, bob)
opt_in_asset(shillings_id, bob)

# Transfer pounds to bob
transfer_asset(pounds_id, 100, alice, bob)

# Get informations
pounds_ = algod_client.asset_info(pounds_id)
pounds_ = algod_client.asset_info(shillings_id)
alice_ = algod_client.account_info(alice["pk"])
bob_ = algod_client.account_info(bob["pk"])

import ipdb; ipdb.set_trace()

from pyteal import *

def coin_exchange(coin_1, coin_2, rate, addr):
    """
    Smart contract to allow coin-exchange by
    a fixed exchange rate.

    Allows a group of 2 AssetTransferTransactions when:
       - x*coin_1 to `addr`, x*rate*coin_2 from `addr`
       - x*rate*coin_2 to `addr`, x*coin_1 from `addr`
    """

    # Transaction type and receivers
    coin_exchange_core = And(
        Gtxn.fee(1) <= Int(10000),
        # 2 AssetTransfer transactions
        Global.group_size() == Int(2),
        Gtxn.type_enum(0) == Int(4),
        Gtxn.type_enum(1) == Int(4),
        # Check that the receivers is the coin-exchange address
        Gtxn.asset_receiver(0) == Addr(addr),
        Gtxn.asset_receiver(1) == Gtxn.sender(0),
    )


    # x*coin_1 -> x*rate*coin_2
    coin_exchange1_to_2 = And(
        Gtxn.xfer_asset(0) == Int(coin_1),
        Gtxn.xfer_asset(1) == Int(coin_2),
        Gtxn.asset_amount(0) * Int(rate) == Gtxn.asset_amount(1),
    )

    # x*rate*coin_2 -> x*coin_1
    coin_exchange2_to_1 = And(
        Gtxn.xfer_asset(0) == Int(coin_2),
        Gtxn.xfer_asset(1) == Int(coin_1),
        Gtxn.asset_amount(0) == Gtxn.asset_amount(1) * Int(rate),
    )

    coin_exchange = And(
        coin_exchange_core, Or(coin_exchange1_to_2, coin_exchange2_to_1)
    )
    return coin_exchange.teal()

import os
import uuid
import pyteal


def compile_teal(teal_source):
    """Invokes the `goal` cli tool and compiles 
    the teal program.
    """

    path = "__teal__/"
    if not os.path.exists(path):
        os.makedirs(path)

    # compile teal
    teal_file = path + str(uuid.uuid4()) + ".teal"
    with open(teal_file, "w+") as f:
        f.write(teal_source)
    lsig_fname = path + str(uuid.uuid4()) + ".lsig"

    # `goal clerk compile` reads a TEAL contract program and 
    # compiles it to binary output and contract address.
    # If the wallet is setup it can also be signed here
    stdout, stderr = pyteal.execute(
        [
            "/home/brns/node/goal", # adjust to setup
            "clerk",
            "compile",
            "-o",
            lsig_fname,
            "-d",
            "/home/brns/node/testnetdata",
            teal_file,
        ]
    )

    if stderr != "":
        print(stderr)
        raise
    elif len(stdout) < 59:
        print("error in compile teal")
        raise

    with open(lsig_fname, "rb") as f:
        teal_bytes = f.read()
    return teal_bytes

print("Create Smart Contract")

# Create TEAL contract: 20 shilling == 1 pound
teal_source = coin_exchange(pounds_id, shillings_id, 20, alice["pk"])

# Create logic signature
teal_bytes = compile_teal(teal_source)
logic_sig = transaction.LogicSig(teal_bytes)

# Allow account delegation by signing the contract
logic_sig.sign(alice["sk"])

def get_exchange_txn(coin_from, from_amount, coin_to, to_amount):

    # Send coins to exchange_addr
    txn1 = transaction.AssetTransferTxn(
        sender=bob["pk"],
        receiver=alice["pk"],
        index=coin_from,
        amt=from_amount,
        flat_fee=True,
        **network_params
    )

    # Dispose coins from exchange_addr
    txn2 = transaction.AssetTransferTxn(
        sender=alice["pk"],
        receiver=bob["pk"],
        index=coin_to,
        amt=to_amount,
        flat_fee=True,
        **network_params
    )

    # get group id and assign it to transactions
    gid = transaction.calculate_group_id([txn1, txn2])
    txn1.group = gid
    txn2.group = gid

    # sign transactions
    stxn1 = txn1.sign(bob["sk"])
    stxn2 = transaction.LogicSigTransaction(txn2, logic_sig)

    signed_group = [stxn1, stxn2]
    return signed_group

print("\nChange: 10p -> 200s")
txn1 = get_exchange_txn(pounds_id, 10, shillings_id, 200)
sent = algod_client.send_transactions(txn1)
wait_for_confirmation(algod_client, sent)

print("\nChange: 20s -> 1p")
txn2 = get_exchange_txn(shillings_id, 20, pounds_id, 1)
sent = algod_client.send_transactions(txn2)
wait_for_confirmation(algod_client, sent)

try:
    print("\nChange: 10s -> 10p")
    # This is the wrong rate and will fail the validation

    txn3 = get_exchange_txn(shillings_id, 10, pounds_id, 10)
    sent = algod_client.send_transactions(txn3)
    wait_for_confirmation(algod_client, sent)
except Exception as e:
    print(e)
