#!/usr/bin/env python3
# two transactions sent atomically
# Second transaction uses the teal script
from pyteal import *

# flake8: noqa
# template variables
tmpl_fee = Int(1000)
tmpl_lease = Bytes("base64", "y9OJ5MRLCHQj8GqbikAUKMBI7hom+SOj8dlopNdNHXI=")
tmpl_amt = Int(200000)
tmpl_rcv = Addr("ZZAF5ARA4MEC5PVDOP64JM5O5MQST63Q2KOY2FLYFLXXD3PFSNJJBYAFZM")
tmpl_fv = Int(55555)
tmpl_lv = Int(56555)
tmpl_cls = Addr("GD64YIY3TWGDMCNPP553DZPPR6LDUSFQOIJVFDPPXWEG3FVOJCCDBBHU5A")


def asset_exchange(asset_1, asset_2, rate, exchange_addr):
    """WIP
    Smart contract to allow exchanging one asset for another by
    a fixed exchange rate.

    Docs: https://pyteal.readthedocs.io/en/latest/atomic_transfer.html

    """

    asset_exchange_core = And(
        Gtxn.fee(1) <= Int(1000),
        # Check that we have 2 AssetTransfer transactions
        Global.group_size() == Int(2),
        Gtxn.type_enum(0) == Int(4),
        Gtxn.type_enum(1) == Int(4),
        # Check receivers
        Gtxn.asset_receiver(0) == Addr(exchange_addr),
        Gtxn.asset_receiver(1) == Gtxn.sender(0),
    )
    asset_exchange1_to_2 = And(
        Gtxn.xfer_asset(0) == Int(asset_1),
        Gtxn.xfer_asset(1) == Int(asset_2),
        Gtxn.asset_amount(0) * Int(rate) == Gtxn.asset_amount(1),
    )

    asset_exchange2_to_1 = And(
        Gtxn.xfer_asset(0) == Int(asset_2),
        Gtxn.xfer_asset(1) == Int(asset_1),
        Gtxn.asset_amount(0) == Gtxn.asset_amount(1) * Int(rate),
    )

    asset_exchange = And(
        asset_exchange_core, Or(asset_exchange1_to_2, asset_exchange2_to_1)
    )

    print(asset_exchange.teal())

    return asset_exchange


def dynamic_fee(
    tmpl_cls=tmpl_cls,
    tmpl_fv=tmpl_fv,
    tmpl_lv=tmpl_lv,
    tmpl_lease=tmpl_lease,
    tmpl_amt=tmpl_amt,
    tmpl_rcv=tmpl_rcv,
):

    dynamic_fee_core = And(
        Global.group_size() == Int(2),
        Gtxn.type_enum(0) == Int(1),  # is payment transaction
        Gtxn.receiver(0) == Txn.sender(),
        Gtxn.amount(0) == Txn.fee(),
        Txn.group_index() == Int(1),
        Txn.type_enum() == Int(1),
        Txn.receiver() == tmpl_rcv,
        Txn.close_remainder_to() == tmpl_cls,
        Txn.amount() == tmpl_amt,
        Txn.first_valid() == tmpl_fv,
        Txn.last_valid() == tmpl_lv,
        Txn.lease() == tmpl_lease,
    )

    return dynamic_fee_core
