from algosdk import account, mnemonic, algod
import uuid
from decouple import config

GOAL_PATH = config("GOAL_PATH")

_algod_client = None
_network_params = None


def get_account():
    # Get the api key from `.env`
    passphrase = config("ALGORAND_MNEMONIC")
    address = config("ALGORAND_ADDRESS")
    return address, passphrase


def create_algod_client():
    """Connect to the node."""

    algod_client = algod.AlgodClient(config("ALGOD_TOKEN"), config("ALGOD_ADDRESS"))

    params = algod_client.suggested_params()

    network_params = {
        "gh": params["genesishashb64"],
        "first": params["lastRound"],
        "last": params["lastRound"] + 1000,
        "fee": params.get("minFee", 1000),
    }
    return algod_client, network_params


def get_algod_client():
    global _algod_client
    global _network_params
    if not _algod_client:

        _algod_client, _network_params = create_algod_client()
    return _algod_client, _network_params


def generate_algorand_keypair():
    private_key, address = account.generate_account()
    print("My address: {}".format(address))
    print("My passphrase: {}".format(mnemonic.from_private_key(private_key)))


def check_account_balance(passphrase):
    private_key = mnemonic.to_private_key(passphrase)
    my_address = mnemonic.to_public_key(passphrase)
    print("My address: {}".format(my_address))

    account_info = algod_client.account_info(my_address)
    print("Account balance: {} microAlgos".format(account_info.get("amount")))


def get_account_info(address=None):
    if not address:
        address = config("ALGORAND_ADDRESS")
    algod_client = create_client()
    return algod_client.account_info(address)


# utility for waiting on a transaction confirmation
def wait_for_confirmation(algod_client, txid):
    while True:
        txinfo = algod_client.pending_transaction_info(txid)
        if txinfo.get("round") and txinfo.get("round") > 0:
            print(
                "Transaction {} confirmed in round {}.".format(
                    txid, txinfo.get("round")
                )
            )
            return txinfo
        else:
            print("Waiting for confirmation...")
            algod_client.status_after_block(algod_client.status().get("lastRound") + 1)


import pyteal

import os


def compile_teal(teal_source):
    path = "__teal__/"
    if not os.path.exists(path):
        os.makedirs(path)

    # compile teal
    teal_file = path + str(uuid.uuid4()) + ".teal"
    with open(teal_file, "w+") as f:
        f.write(teal_source)
    lsig_fname = path + str(uuid.uuid4()) + ".tealc"

    # TODO read goal path from .env
    stdout, stderr = pyteal.execute(
        [GOAL_PATH, "clerk", "compile", "-o", lsig_fname, teal_file]
    )

    if stderr != "":
        print(stderr)
        raise
    elif len(stdout) < 59:
        print("error in compile teal")
        raise

    with open(lsig_fname, "rb") as f:
        teal_bytes = f.read()
    return teal_bytes
