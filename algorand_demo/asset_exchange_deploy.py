# /usr/bin/python3
import json
import time
import base64
import os
from pyteal import *
import uuid, base64
from algosdk import algod, transaction, account, mnemonic
from asset_exchange import dynamic_fee, asset_exchange
import helper
import asset_transfer


# group transactions
def group_transactions():

    # recover a account
    passphrase = "argue vessel table ring oak quiz boat beef hire letter inquiry upset delay coast quick poem loan curve maid script rare ice weasel absent radio"
    pk_account_a = mnemonic.to_private_key(passphrase)
    account_a = account.address_from_private_key(pk_account_a)

    # recover b account
    passphrase2 = "snake unveil hello input club barrel measure announce bring seed practice enact train camp enlist wear kick science now word horror month rather abstract course"
    pk_account_b = mnemonic.to_private_key(passphrase2)
    account_b = account.address_from_private_key(pk_account_b)

    # recover c account
    passphrase3 = "salmon about chair clap body sample chuckle inside adapt leg bonus afford grit floor pencil celery cherry armor solar sheriff loyal vessel stay absorb budget"
    pk_account_c = mnemonic.to_private_key(passphrase3)
    account_c = account.address_from_private_key(pk_account_c)

    # connect to node
    algod_client, network_params = helper.get_algod_client()

    asset_holder = {}
    asset_holder["pk"] = mnemonic.to_public_key(passphrase)
    asset_holder["sk"] = mnemonic.to_private_key(passphrase)

    asset1 = 400024
    asset2 = 406534
    asset1 = asset1 or asset_transfer.create_asset(
        "Pounds", "Pound", asset_holder, algod_client, network_params
    )
    asset2 = asset2 or asset_transfer.create_asset(
        "Shillings", "Shilling", asset_holder, algod_client, network_params
    )

    #####################
    # Create contract
    teal_source = asset_exchange(
        asset_1=asset1, asset_2=asset2, rate=20, exchange_addr=asset_holder["pk"]
    ).teal()

    teal_bytes = helper.compile_teal(teal_source)
    lsig = transaction.LogicSig(teal_bytes)
    lsig.sign(asset_holder["sk"])

    import ipdb

    ipdb.set_trace()
    # TODO use second account and opt in
    user = asset_holder
    txn1 = transaction.AssetTransferTxn(
        sender=user["pk"],
        receiver=asset_holder["pk"],
        amt=1,
        index=asset1,
        flat_fee=True,
        **network_params
    )

    txn2 = transaction.AssetTransferTxn(
        sender=asset_holder["pk"],
        receiver=user["pk"],
        amt=20,
        index=asset2,
        flat_fee=True,
        **network_params
    )

    # get group id and assign it to transactions
    gid = transaction.calculate_group_id([txn1, txn2])
    txn1.group = gid
    txn2.group = gid

    # sign transaction1
    stxn1 = txn1.sign(user["sk"])

    # sign transaction2
    stxn2 = transaction.LogicSigTransaction(txn2, lsig)

    transaction.write_to_file([stxn1, stxn2], "simple.stxn")
    signedGroup = []
    signedGroup.append(stxn1)
    signedGroup.append(stxn2)

    # send them over network
    sent = algod_client.send_transactions(signedGroup)
    # print txid
    print(sent)

    # wait for confirmation
    helper.wait_for_confirmation(algod_client, sent)


# Run
group_transactions()
