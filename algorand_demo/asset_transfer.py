"""https://github.com/PureStake/api-examples/blob/master/python-examples/complete_example.py"""

import hashlib
import json
import secrets

import ipdb
from decouple import config

# your terminal output should look similar to the following
# Account 1 account: THQHGD4HEESOPSJJYYF34MWKOI57HXBX4XR63EPBKCWPOJG5KUPDJ7QJCM
# Account 1 account: AJNNFQN7DSR7QEY766V7JDG35OPM53ZSNF7CU264AWOOUGSZBMLMSKCRIU
# Account 1 account: 3ZQ3SHCYIKSGK7MTZ7PE7S6EDOFWLKDQ6RYYVMT7OHNQ4UJ774LE52AQCU
from algosdk import account, algod, encoding, kmd, mnemonic, transaction
import helper

# from algosdk.future import transaction

# Setup HTTP client w/guest key provided by PureStake
algod_token = config("PURESTACK_API_KEY")  # Get the api key from `.env`
algod_address = "https://testnet-algorand.api.purestake.io/ps1"
purestake_token = {"X-Api-key": algod_token}  # header


# Shown for demonstration purposes. NEVER reveal secret mnemonics in practice.
# Change these values with your mnemonics
# mnemonic1 = "PASTE your phrase for account 1"
# mnemonic2 = "PASTE your phrase for account 2"
# mnemonic3 = "PASTE your phrase for account 3"

mnemonic1 = "portion never forward pill lunch organ biology weird catch curve isolate plug innocent skin grunt bounce clown mercy hole eagle soul chunk type absorb trim"
mnemonic2 = "place blouse sad pigeon wing warrior wild script problem team blouse camp soldier breeze twist mother vanish public glass code arrow execute convince ability there"
mnemonic3 = "image travel claw climb bottom spot path roast century also task cherry address curious save item clean theme amateur loyal apart hybrid steak about blanket"

# For ease of reference, add account public and private keys to
# an accounts dict.
accounts = {}
counter = 1
for m in [mnemonic1, mnemonic2, mnemonic3]:
    accounts[counter] = {}
    accounts[counter]["pk"] = mnemonic.to_public_key(m)
    accounts[counter]["sk"] = mnemonic.to_private_key(m)
    counter += 1

# Initialize an algod client
algod_client, network_params = helper.get_algod_client()

#############
# CODE

# Get network params for transaction
params = algod_client.suggested_params()
first = params.get("lastRound")
last = first + 1000
gen = params.get("genesisID")
gh = params.get("genesishashb64")
min_fee = params.get("minFee")

# Utility


def create_asset(
    asset_name, unit_name, account, algod_client=None, network_params=None
):
    """Create an asset that is hold by the account."""

    # Configure fields for creating the asset.
    data = {
        "sender": account["pk"],
        "total": 1000,
        "decimals": 0,
        "default_frozen": False,
        "unit_name": unit_name,
        "asset_name": asset_name,
        "manager": account["pk"],
        "reserve": account["pk"],
        "freeze": account["pk"],
        "clawback": account["pk"],
        "url": "https://path/to/my/asset/details",
        "flat_fee": True,
    }
    data.update(network_params)

    # Construct Asset Creation transaction
    txn = transaction.AssetConfigTxn(**data)

    # Sign with secret key of creator
    stxn = txn.sign(account["sk"])

    # Send the transaction to the network and retrieve the txid.
    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )

    # Wait for the transaction to be confirmed
    txinfo = helper.wait_for_confirmation(algod_client, txid)

    asset_id = txinfo["txresults"]["createdasset"]

    print("Created Asset: {} {}".format(asset_name, asset_id))

    return asset_id


def opt_in_asset(asset_id, account):
    # Get latest network parameters
    data = {
        "sender": account["pk"],
        "fee": min_fee,
        "first": first,
        "last": last,
        "gh": gh,
        "receiver": account["pk"],
        "amt": 0,
        "index": asset_id,
        "flat_fee": True,
    }
    print("Asset Option In")
    # Use the AssetTransferTxn class to transfer assets
    txn = transaction.AssetTransferTxn(**data)
    stxn = txn.sign(accounts[2]["sk"])
    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )
    print(txid)
    # Wait for the transaction to be confirmed
    helper.wait_for_confirmation(algod_client, txid)
    # Now check the asset holding for that account.
    # This should now show a holding with a balance of 0.
    account_info = algod_client.account_info(accounts[2]["pk"])
    print(json.dumps(account_info["assets"][str(asset_id)], indent=4))


def transfer_asset(asset_id, sender, receiver):
    # send 10
    data = {
        "sender": sender["pk"],
        "fee": min_fee,
        "first": first,
        "last": last,
        "gh": gh,
        "receiver": receiver["pk"],
        "amt": 10,
        "index": asset_id,
        "flat_fee": True,
    }
    print("Asset Transfer")
    txn = transaction.AssetTransferTxn(**data)
    stxn = txn.sign(sender["sk"])
    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )
    print(txid)
    # Wait for the transaction to be confirmed
    helper.wait_for_confirmation(algod_client, txid)
    # The balance should now be 10.
    account_info = algod_client.account_info(receiver["pk"])
    print(json.dumps(account_info["assets"][str(asset_id)], indent=4))


# Not cleaned examples


def update_asset(asset_id):

    # Update manager address.
    # Keep reserve, freeze, and clawback address same as before, i.e. account 1
    data = {
        "sender": accounts[1]["pk"],
        "fee": min_fee,
        "first": first,
        "last": last,
        "gh": gh,
        "index": asset_id,
        "manager": accounts[2]["pk"],
        "reserve": accounts[1]["pk"],
        "freeze": accounts[1]["pk"],
        "clawback": accounts[1]["pk"],
        "flat_fee": True,
    }
    txn = transaction.AssetConfigTxn(**data)
    stxn = txn.sign(accounts[1]["sk"])
    print("Asset Modification")
    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )
    print(txid)

    # Wait for the transaction to be confirmed
    helper.wait_for_confirmation(algod_client, txid)

    # Check asset info to view change in management.
    asset_info = algod_client.asset_info(asset_id)
    print(json.dumps(asset_info, indent=4))


def freeze_asset(asset_id):

    # Freezing an Asset
    data = {
        "sender": accounts[1]["pk"],
        "fee": min_fee,
        "first": first,
        "last": last,
        "gh": gh,
        "index": asset_id,
        "target": accounts[2]["pk"],
        "new_freeze_state": True,
        "flat_fee": True,
    }
    print("Asset Freeze")
    txn = transaction.AssetFreezeTxn(**data)
    stxn = txn.sign(accounts[1]["sk"])
    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )
    print(txid)
    # Wait for the transaction to be confirmed
    helper.wait_for_confirmation(algod_client, txid)
    # The balance should now be 10.
    account_info = algod_client.account_info(accounts[2]["pk"])
    print(json.dumps(account_info["assets"][str(asset_id)], indent=4))


def revoke_asset(asset_id):
    # Revoking an Asset
    data = {
        "sender": accounts[1]["pk"],
        "fee": min_fee,
        "first": first,
        "last": last,
        "gh": gh,
        "receiver": accounts[1]["pk"],
        "amt": 10,
        "index": asset_id,
        "revocation_target": accounts[2]["pk"],
        "flat_fee": True,
    }
    print("Asset Revoke")
    txn = transaction.AssetTransferTxn(**data)
    stxn = txn.sign(accounts[1]["sk"])
    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )
    print(txid)
    # Wait for the transaction to be confirmed
    helper.wait_for_confirmation(algod_client, txid)
    # The balance of account 2 should now be 0.
    account_info = algod_client.account_info(accounts[2]["pk"])
    print(json.dumps(account_info["assets"][str(asset_id)], indent=4))
    # The balance of account 1 should increase by 10 to 1000.
    account_info = algod_client.account_info(accounts[1]["pk"])
    print(json.dumps(account_info["assets"][str(asset_id)], indent=4))


def destroy_asset(asset_id):
    # Destroy Asset
    data = {
        "sender": accounts[2]["pk"],
        "fee": min_fee,
        "first": first,
        "last": last,
        "gh": gh,
        "index": asset_id,
        "flat_fee": True,
        "strict_empty_address_check": False,
    }
    print("Destroying Asset")
    # Construct Asset Creation transaction
    txn = transaction.AssetConfigTxn(**data)

    # Sign with secret key of creator
    stxn = txn.sign(accounts[2]["sk"])

    # Send the transaction to the network and retrieve the txid.
    txid = algod_client.send_transaction(
        stxn, headers={"content-type": "application/x-binary"}
    )
    print(txid)

    # Wait for the transaction to be confirmed
    helper.wait_for_confirmation(algod_client, txid)

    # This should raise an exception since the asset was deleted.
    try:
        asset_info = algod_client.asset_info(asset_id)
    except Exception as e:
        print(e)


if __name__ == "__main__":
    asset_id = create_asset("Token", "TKN", accounts[1], network_params)
    update_asset(asset_id)
    # Check if asset_id is in account 2's asset holdings prior to opt-in
    account_info = algod_client.account_info(accounts[2]["pk"])
    holding = None
    if "assets" in account_info:
        holding = account_info["assets"].get(str(asset_id))

    if not holding:
        opt_in_asset(asset_id, accounts[2])
    transfer_asset(asset_id, accounts[1], accounts[2])
    freeze_asset(asset_id)
    revoke_asset(asset_id)
    destroy_asset(asset_id)

    choice = 1
    nonce = secrets.token_hex(32)
    hash_ = hashlib.sha256()
    hash_.update(nonce.encode("utf-8"))
    hash_.update(str(choice).encode("utf-8"))

    print(hash_)
