flake8==3.7.9
ipdb==0.13.2
ipython==7.13.0
mypy==0.770
pytest==5.4.1

py-algorand-sdk==1.2.1
python-decouple==3.3
pyteal==0.5.3
params==0.9.0
